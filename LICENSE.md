# End-User License Agreement for KeytroL.

This End-User License Agreement ("EULA") is a legal agreement between you (either an individual or a single entity), the end-user, and Alain Martin, the author of KeytroL ("AUTHOR"). This EULA permits you to use the software product identified above, which includes computer software and may include associated software components, media, printed materials, and "online" or electronic documentation ("SOFTWARE PRODUCT").

By installing, copying, or otherwise using the SOFTWARE PRODUCT, you agree to be bound by the terms of this EULA. If you do not agree to the terms of this EULA, do not install or use the SOFTWARE PRODUCT.

The SOFTWARE PRODUCT is protected by copyright laws and international copyright treaties, as well as other intellectual property laws and treaties. The SOFTWARE PRODUCT is licensed, not sold.

## 1. GRANT OF LICENSE.

The SOFTWARE PRODUCT is provided as "freeware" without charge. You are granted a non-exclusive license to use the SOFTWARE PRODUCT for any purposes for an unlimited period of time.

## 2. DISTRIBUTION.

You may make copies of the SOFTWARE PRODUCT as you wish; give exact copies of the original SOFTWARE PRODUCT to anyone; and distribute the SOFTWARE PRODUCT in its unmodified form via electronic means. You may not charge money or fees for the SOFTWARE PRODUCT, except to cover distribution costs. You must distribute a copy of this EULA with any copy of the SOFTWARE PRODUCT and anyone to whom you distribute the SOFTWARE PRODUCT is subject to this EULA.

## 3. DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS.

### 3.1. Maintenance of Copyright Notices.

You must not remove or alter any copyright notices on any and all copies of the SOFTWARE PRODUCT.

### 3.2. Prohibition on Reverse Engineering, Decompilation, and Disassembly.

You may not reverse engineer, decompile, or disassemble the SOFTWARE PRODUCT, except and only to the extent that such activity is expressly permitted by applicable law notwithstanding this limitation.

### 3.3. Rental.

You may not rent, lease, or lend the SOFTWARE PRODUCT.

## 4. TERMINATION.

Without prejudice to any other rights, the AUTHOR may terminate this EULA if you fail to comply with the terms and conditions of this EULA. In such event, you must destroy all copies of the SOFTWARE PRODUCT and all of its component parts.

## 5. NO WARRANTIES.

The AUTHOR expressly disclaims any warranty for the SOFTWARE PRODUCT. The SOFTWARE PRODUCT and any related documentation is provided "AS IS" without warranty of any kind, either express or implied, including, without limitation, the implied warranties or merchantability, fitness for a particular purpose, or noninfringement. The entire risk arising out of use or performance of the SOFTWARE PRODUCT remains with you.

## 6. LIMITATION OF LIABILITY.

In no event shall the AUTHOR be liable for any special, consequential, incidental or indirect damages whatsoever (including, without limitation, damages for loss of business profits, business interruption, loss of business information, or any other pecuniary loss) arising out of the use of or inability to use this product, even if the AUTHOR is aware of the possibility of such damages and known defects.

## 7. CREDITS.

This SOFTWARE PRODUCT includes icons from or adapted from those in the [FatCow "Farm-Fresh Web Icons" icon sets](http://www.fatcow.com/free-icons). These icon sets are licensed under a [Creative Commons Attribution 3.0 License](http://creativecommons.org/licenses/by/3.0/us/).
