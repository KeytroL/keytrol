# KeytroL

KeytroL is a software application for turning your computer keyboard into a MIDI controller.

## Download

Latest Release (15 May 2013): [Version 0.1.0](https://bitbucket.org/KeytroL/keytrol/downloads/KeytroL-0.1.0-setup.exe)

## Copyright and License

Copyright 2011-2013 Alain Martin and Matthieu Talbot.

Licensed under KeytroL EULA. Please read *LICENSE.md* for further details.